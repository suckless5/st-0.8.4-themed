# st-0.8.4-themed
Custom configured st version **0.8.4** program to look pretty and easy on the eyes.

## screenshot

### Linux Mint
![](/screenshot/st-terminal-linuxmint.png)

## st desktop file location
```
$HOME/.local/share/applications
```
``
$HOME=/home/user
``

## Ubuntu dependencies
1. build-essential 
2. libx11-dev
3. libxft-dev

## Arch dependencies
1. base-dev
2. libx11
3. libxft

### Terminal Theme
***Dracula***<br>
https://github.com/dracula/dracula-theme <br>
  
## Patches
1. dynamic-cursor-color: https://st.suckless.org/patches/dynamic-cursor-color/
2. Scrollback patch sucks.

## tmux
Use tmux to have proper scroll history buffer support.
